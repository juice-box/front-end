FROM nginx:alpine
RUN apt-get update -y
RUN apt-get install node
COPY /build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]