import React from 'react';
import logo from './logo.svg';
import './App.css';
import Graph from './Components/AnimatedLine';
import  Line from './Components/Line';
import Data from './Components/Fetch';
import AnimatedLine from './Components/AnimatedLine';
import StaticLine from './Components/StaticLine';
import Table from './Components/Table'

const INSTRUMENTS = [
	{ name: "Astronomica", price: ["94303"] },
	{ name: "Borealis", price: ["94088"] },
	{ name: "Celestial", price: [95062, 2838, 423245] },
	{ name: "Deuteronic", price: ["96803"] },
	{ name: "Eclipse", price: ["94303"] },
	{ name: "Floral", price: ["94088"] },
	{ name: "Galactia", price: ["95062"] },
	{ name: "Heliosphere", price: ["96803"] },
	{ name: "Interstella", price: ["94303"] },
	{ name: "Jupiter", price: ["94088"] },
	{ name: "Koronis", price: ["95062"] },
	{ name: "Lunatic", price: ["96803"] }
  ];

  const TABLEDATA = [
	{instrumentName: "Galactia", cpty: "Lewis", price: 9964.235074757127, type: "S", quantity: 71, time: "11-Aug-2019 (12:07:06.471252)"},
	{instrumentName: "Deuteronic", cpty: "Lewis", price: 8540.619061084186, type: "B", quantity: 2, time: "11-Aug-2019 (12:07:06.669213)"},
	{instrumentName: "Lunatic", cpty: "John", price: 1830.8271150278415, type: "B", quantity: 1, time: "11-Aug-2019 (12:07:06.944790)"}
	/*{instrumentName: "Astronomica", "cpty": "Nidia", "price": 3409.1510219490383, "type": "S", "quantity": 3, "time": "11-Aug-2019 (12:07:07.042573)"},
	{instrumentName: "Heliosphere", "cpty": "Lina", "price": 7748.8136364925085, "type": "S", "quantity": 430, "time": "11-Aug-2019 (12:07:07.106903)"},
	{instrumentName: "Floral", "cpty": "Lina", "price": 392.68752101888583, "type": "S", "quantity": 157, "time": "11-Aug-2019 (12:07:07.222031)"},
	{instrumentName: "Interstella", "cpty": "Selvyn", "price": 3569.218762415197, "type": "B", "quantity": 17, "time": "11-Aug-2019 (12:07:07.512138)"},
	{instrumentName: "Lunatic", "cpty": "Lina", "price": 1829.1557276459944, "type": "S", "quantity": 18, "time": "11-Aug-2019 (12:07:07.653035)"},
	{instrumentName: "Eclipse", "cpty": "Richard", "price": 9858.733663910441, "type": "S", "quantity": 273, "time": "11-Aug-2019 (12:07:07.666203)"},
	{instrumentName: "Lunatic", "cpty": "Selvyn", "price": 1845.6411973911347, "type": "B", "quantity": 434, "time": "11-Aug-2019 (12:07:07.747054)"},
	{instrumentName: "Galactia", "cpty": "Lewis", "price": 10051.25633626954, "type": "B", "quantity": 11, "time": "11-Aug-2019 (12:07:07.876999)"},
	{instrumentName: "Galactia", "cpty": "John", "price": 8540.619061084186, "type": "B", "quantity": 31, "time": "11-Aug-2019 (12:15:26.747054)"},
	{instrumentName: "Galactia", "cpty": "Lina", "price": 9569.218762415197, "type": "S", "quantity": 20, "time": "11-Aug-2019 (12:20:07.512138)"},
	{instrumentName: "Galactia", "cpty": "Richard", "price": 8748.8136364925085, "type": "S", "quantity": 10, "time": "11-Aug-2019 (12:35:07.222031)"}*/
   
   ];


class LineDisplay extends React.Component {


	render() {
			return this.props.price
			//.map((price) => price + "	")
			//return this.props.price.map(function(price))
			//return ( <h1>For instrument {this.props.name} price is {this.props.price[i]}</h1>);
	}
  }


export default class App extends React.Component {

	/*constructor(props){
		super(props);
		//tableData = fetch('https://jsonplaceholder.typicode.com/users').then(response => response.json())
		//this.state = tableData
		this.state={
		  
		   
		//   tableData2:[
		//     {'Name': 'Abc', 'Age': 15, 'Location': 'Bangalore'},
		//     {'Name': 'Def', 'Age': 43, 'Location': 'Mumbai'},
		//     {'Name': 'Uff', 'Age': 30, 'Location': 'Chennai'},
		//     {'Name': 'Ammse', 'Age': 87, 'Location': 'Delhi'},
		//     {'Name': 'Yysse', 'Age': 28, 'Location': 'Hyderabad'}
		// ]
		};*/


	constructor() {
		super();
		this.state = {
		  activeInstrument: 0,
		};
	  }

	  render() {
		  const activeInstrument = this.state.activeInstrument;
		//   const items = []
		// 	for(const [index, value] of INSTRUMENTS.entries()) {
		// 		if(value.instrumentName == activeInstrument.instrumentName){
		// 			items.push(value)
		// 		}
		// 	}
		  return (
			<div className="App">
    		{/*<StaticLine  />*/}
    		{INSTRUMENTS.map((instrument, index) => (
      		<button
        		key={index}
        		onClick={() => {
					this.setState({ activeInstrument: index })
        		}}
      		>
          		{instrument.instrumentName}
      		</button>
			
			))}
			<br></br>


			{/*<LineDisplay
          key={activeInstrument}
		  name={TABLEDATA[activeInstrument].instrumentName}
		  price={TABLEDATA[activeInstrument].price}
			/>*/}
			{/*<Table data={items}/>*/}
			{/* <LineDisplay /> */}
			<StaticLine />
			<AnimatedLine />
			
  		</div>
	  );
	  }

	  /*render() {
		const activeInstrument = this.state.activeInstrument;
	  return (
		<div className="App">
    		{/*<StaticLine  />*///}
    		/*{INSTRUMENTS.map((instrument, index) => (
      		<button
        		key={index}
        		onClick={() => {
					this.setState({ activeInstrument: index })
        		}}
      		>
          		{instrument.name}
      		</button>
			
			))}
			<br></br>
			<LineDisplay
          key={activeInstrument}
		  name={INSTRUMENTS[activeInstrument].name}
		  price={INSTRUMENTS[activeInstrument].price}
        />
			
  		</div>
	  );
	  }*/
	}

